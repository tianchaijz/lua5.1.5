#!/bin/bash

patch_() {
    patch -p1 < lua-arch.patch
    patch -p1 < lua-5.1-cflags.diff
}

checkout_() {
    git checkout Makefile
    git checkout src/Makefile
    git checkout src/luaconf.h
    git checkout etc/lua.pc
}

if [ "$1" == "patch" ]; then
    patch_
    exit 0
fi

if [ "$1" == "co" ]; then
    checkout_
    exit 0
fi

make clean

if [ "$1" == "debug" ]; then
    output=$(git status | grep 'src/Makefile')
    if [ -z "$output" ]; then
        patch_
    fi

    export CFLAGS="$CFLAGS -Wall -fPIC -O0 -g"
else
    export CFLAGS="$CFLAGS -Wall -fPIC"
fi

make MYCFLAGS="$CFLAGS" MYLDFLAGS="$LDFLAGS" linux
